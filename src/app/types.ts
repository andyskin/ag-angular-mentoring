export interface IName {
    first: string;
    last: string;
}

export interface IUser {
    id: string;
    fakeToken: string;
    name: IName;
    login: string;
    password: string;
    token: string;
};

export interface IUserResponse {
    token: string;
}

export interface IAuthor {
    id: number;
    firstName: string;
    lastName: string;
}

export interface ICourse {
    id: string;
    name: string;
    description: string;
    length: number;
    date: Date;
    isTopRated: boolean;
    authors: IAuthor[];
};