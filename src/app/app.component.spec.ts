import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Layout/header/header.component';
import { MainComponent } from './Layout/main/main.component';
import { FooterComponent } from './Layout/footer/footer.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from 'selenium-webdriver';
import { BreadcrumbsComponent } from './Components/breadcrumbs/breadcrumbs.component';
import { CourseComponent } from './Components/course/course.component';
import { CoursesToolboxComponent } from './Components/courses-toolbox/courses-toolbox.component';
import { LogoComponent } from './Components/logo/logo.component';
import { UserPanelComponent } from './Components/user-panel/user-panel.component';
import { FormsModule } from '@angular/forms';
import { CourseListComponent } from './Pages/course-list/course-list.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        FooterComponent,
        LogoComponent,
        UserPanelComponent,
        BreadcrumbsComponent,
        CourseListComponent,
        CoursesToolboxComponent,
        CourseComponent
      ],
      imports: [
        FormsModule
      ]
      // schemas: [
      //   CUSTOM_ELEMENTS_SCHEMA
      // ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  
  it(`should have as title 'AgAngularMentoring'`, async(() => {
    expect(component.title).toEqual('AgAngularMentoring');
  }));
});
