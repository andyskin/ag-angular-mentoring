import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { UserComponent } from './Components/user/user.component';
import { HeaderComponent } from './Layout/header/header.component';
import { MainComponent } from './Layout/main/main.component';
import { FooterComponent } from './Layout/footer/footer.component';
import { LogoComponent } from './Components/logo/logo.component';
import { UserPanelComponent } from './Components/user-panel/user-panel.component';
import { BreadcrumbsComponent } from './Components/breadcrumbs/breadcrumbs.component';
import { CoursesToolboxComponent } from './Components/courses-toolbox/courses-toolbox.component';
import { CourseListComponent } from './Pages/course-list/course-list.component';
import { CourseComponent } from './Components/course/course.component';
import { CoursesService } from './Services/courses/courses.service';
import { HighlightDirective } from './Directives/highlight.directive';
import { DurationPipe } from './Pipes/duration.pipe';
import { CourseSearchPipe } from './Pipes/course-search.pipe';
import { OrderByPipe } from './Pipes/order-by.pipe';
import { LoginComponent } from './Pages/login/login.component';
import { PageNotFoundComponent } from './Pages/page-not-found/page-not-found.component';
import { CourseDetailsComponent } from './Pages/course-details/course-details.component';
import { AuthGuardService } from './Services/auth/auth-guard.service';
import { LoadingComponent } from './Pages/loading/loading.component';
import { TokenInterceptor } from './Services/api/token-interceptor';
import { EffectsModule, Actions } from '@ngrx/effects';
import { StoreModule, ReducerManager, ReducerManagerDispatcher } from '@ngrx/store';

import { courseReducer, CoursesEffects, coursesEffects } from './Store/courses';
import { authReducer, authEffects } from './Store/auth';
import { DateInputComponent } from './Pages/course-details/date-input/date-input.component';
import { DurationInputComponent } from './Pages/course-details/duration-input/duration-input.component';

export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, data: { title: 'Login' } },
  { 
    path: 'courses',
    canActivate: [AuthGuardService],
    children: [
      { path: '', component: CourseListComponent, data: { title: 'Courses' } },
      { path: 'new', component: CourseDetailsComponent, data: { title: 'New Course' } },
      { path: ':id', component: CourseDetailsComponent, data: { title: `Video Course ` } }
    ] 
  },
  { path: '404', component: PageNotFoundComponent, canActivate: [AuthGuardService], data: { title: 'Not Found' } },
  { path: '', redirectTo: '/courses', pathMatch: 'full' },
  { path: '**', redirectTo: '/404', pathMatch: 'full' }
];

const effects = [...coursesEffects, ...authEffects];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    LogoComponent,
    UserPanelComponent,
    BreadcrumbsComponent,
    CourseListComponent,
    CoursesToolboxComponent,
    CourseComponent,
    HighlightDirective,
    DurationPipe,
    CourseSearchPipe,
    OrderByPipe,
    LoginComponent,
    PageNotFoundComponent,
    CourseDetailsComponent,
    LoadingComponent,
    DateInputComponent,
    DurationInputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    StoreModule.forRoot({}),
    StoreModule.forFeature('courses', courseReducer),
    StoreModule.forFeature('users', authReducer),
    EffectsModule.forRoot(effects)
  ],
  providers: [
    CoursesService,
    CourseSearchPipe,
    LoadingComponent,
    ReducerManager,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
