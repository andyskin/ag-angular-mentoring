import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[courseHighlight]'
})
export class HighlightDirective implements OnInit {
  @Input('courseHighlight') creationDate: Date;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    const freshCourseColor = '#7F993A',
      upcomingCourseColor = '#2a2affbd';

    const dateFormat = 'YYYY-MM-DD',
      creationMoment = moment(this.creationDate).format(dateFormat),
      currentDate = moment().format(dateFormat),
      twoWeeksAgo = moment().subtract(2, 'weeks').format(dateFormat);
    
    if (creationMoment > currentDate) {
      this.highlight(upcomingCourseColor);
    }
    else if (
      creationMoment <= currentDate &&
      creationMoment >= twoWeeksAgo
    ) {
      this.highlight(freshCourseColor);
    }
  }

  private highlight(color: string) {
    this.el.nativeElement.style.borderColor = color;
  }

}
