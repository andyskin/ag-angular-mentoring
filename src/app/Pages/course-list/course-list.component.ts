import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../Services/courses/courses.service';
import { ICourse } from '../../types';
import { Store } from '@ngrx/store';
import * as courseStore from '../../Store/courses';
@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  courses: ICourse[];
  isMore: boolean;

  constructor(
    private coursesService: CoursesService,
    private store: Store<courseStore.CoursesState>
  ) {
    this.courses = [];
    this.isMore = true;
  }

  ngOnInit() {
    this.getCourses();

    if(this.courses.length === 0) {
      this.store.dispatch(new courseStore.LoadCourses({
        start: 0,
        count: 6
      }));
    } else if (this.courses.length > 6) {
      this.scrollToNewCourses();
    }
  }

  findCourse(courseName: string) {
    if (!courseName) {
      this.getCourses();
    } else {
      this.coursesService.findCourse(courseName).subscribe((courses: ICourse[]) => {
        this.courses = courses;
      });
    }
  }

  getCourses(toNavigate: boolean = false) {
    this.store.select(courseStore.getAllCourses).subscribe(courses => {
      this.courses = [...courses];

      if(toNavigate) {
        this.scrollToNewCourses();
      }
    });
  }

  deleteCourse(courseId: string) {
    if(confirm("Do you want to remove this course?")) {
      this.store.dispatch(new courseStore.DeleteCourse(courseId));
    }
  }

  loadMore() {
    this.store.dispatch(new courseStore.LoadCourses({
      start: this.courses.length-1
    }));

    this.getCourses(true);
  }

  scrollToNewCourses() {
    requestAnimationFrame(() => {
      const findMoreBtn = document.querySelector('.load-courses');

      if (findMoreBtn) {
        findMoreBtn.scrollIntoView({
          behavior: 'smooth'
        });
      }
    });
  }
}
