import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseListComponent } from './course-list.component';
import { CourseComponent } from '../../Components/course/course.component';
import { CoursesToolboxComponent } from '../../Components/courses-toolbox/courses-toolbox.component';
import { FormsModule } from '@angular/forms';
import { CoursesService } from '../../Services/courses/courses.service';
import { ICourse } from '../../types';

class CoursesServiceStub {
  courses: ICourse[] = [
    {
      "id": "1",
      "name": "Test course 1",
      "date": new Date("04.06.2014"),
      "length": 60,
      "description": "Test course 1 description",
      "isTopRated": false,
      "authors": []
    },
    {
      "id": "2",
      "name": "Test course 2",
      "date": new Date("05.27.2016"),
      "length": 120,
      "description": "Test course 2 description",
      "isTopRated": true,
      "authors": []
    },
    {
      "id": "3",
      "name": "Test course 3",
      "date": new Date("05.14.2015"),
      "length": 60,
      "description": "Test course 3 description",
      "isTopRated": false,
      "authors": []
    },
    {
      "id": "4",
      "name": "Test course 4",
      "date": new Date("01.07.2015"),
      "length": 90,
      "description": "Test course 4 description",
      "isTopRated": false,
      "authors": []
    },
    {
      "id": "5",
      "name": "Test course 5",
      "date": new Date("01.20.2017"),
      "length": 120,
      "description": "Test course 5 description",
      "isTopRated": true,
      "authors": []
    }
  ];

  getCourses = () => {
    return this.courses;
  };
  
  addCourse = (course: ICourse) => {
    this.courses.push(course);
  };

  deleteCourse = (id: string) => {
    this.courses = this.courses.filter(
      course => course.id !== id
    );
  }
}

describe('CourseListComponent', () => {
  let component: CourseListComponent;
  let fixture: ComponentFixture<CourseListComponent>;
  let coursesServiceStub: Partial<CoursesService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CourseListComponent,
        CourseComponent,
        CoursesToolboxComponent
      ],
      providers: [
        CourseComponent,
        { provide: CoursesService, useClass: CoursesServiceStub }
      ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have Load More button', () => {
    expect(fixture.nativeElement.querySelector('button.load-courses'))
      .toBeTruthy();
  });

  it('#courses should not be empty', () => {
    expect(component.courses).toBeTruthy();
  });
});
