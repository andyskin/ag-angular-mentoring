import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../Services/auth/auth.service';
import { Router } from '@angular/router';
import { IUser, IUserResponse } from '../../types';
import { Store } from '@ngrx/store';

import * as authStore from '../../Store/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private store: Store<authStore.AuthState>
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [ Validators.required ]),
      password: new FormControl('', [ Validators.required ])
    });
  }

  ngOnInit() {}

  login(): void {
    const { username, password } = this.loginForm.value;

    this.authService.login(username, password).subscribe((response: IUserResponse) => {
      if (response) this.getUserInfo(response.token);
    })
  }

  getUserInfo(token: string): void {
    this.store.dispatch(new authStore.Login(token));
    this.store.select(authStore.getUser).subscribe(user => {
      this.authService.setCurrentUser(user)
    });
  }

  getCurrentUser() {
    let currentUser = null;

    this.store.select(authStore.getUser).subscribe(user => {
      currentUser = user ? user : this.initUserFromStorage()
    });

    return currentUser;
  }

  initUserFromStorage() {
    let currentUser = this.authService.getCurrentUserFromStorage();

    if (currentUser) {
      this.store.dispatch(
        new authStore.LoginSuccess(currentUser)
      );
    }

    return currentUser;
  }
}
