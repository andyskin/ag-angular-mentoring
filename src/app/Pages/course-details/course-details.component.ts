import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Course } from '../../Services/courses/course.model';
import * as nanoid from 'nanoid';
import * as moment from 'moment';
import * as courseStore from '../../Store/courses';
import { Store } from '@ngrx/store';
import { FormGroup, FormControl, Validators, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ApiService } from '../../Services/api/api.service';
import { dateExistsValidator, dateFormatValidator, dateLengthValidator } from 'src/app/Validators/date.validator';
import { durationTypeValidator } from 'src/app/Validators/duration.validator';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {

  private dateFormat = 'MM/DD/YYYY';
  private dateRe = new RegExp('.{2}\/.{2}\/.{4}');

  course: Course;
  title: string;
  duration: number;
  description: string;
  date: Date;

  public courseForm: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private store: Store<courseStore.CoursesState>
  ) {
    this.setForm();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const courseId = params["id"];

      if (courseId) {
        this.getCourse(courseId);
      }
    });
  }

  setForm() {
    this.courseForm = new FormGroup({
      title: new FormControl(this.title, [ Validators.required, Validators.maxLength(50) ]),
      description: new FormControl(this.description, [ Validators.required, Validators.maxLength(500) ]),
      duration: new FormControl(this.duration, [ 
        Validators.required,
        durationTypeValidator()
      ]),
      date: new FormControl(this.date, [
        Validators.required,
        dateExistsValidator(this.dateFormat),
        dateFormatValidator(this.dateRe),
        dateLengthValidator(this.dateFormat.length)
      ])
    });
  }

  cancel() {
    this.router.navigateByUrl("/");
  }

  getCourse(id: string) {
    this.api.getCourse(id).subscribe(course => {
      this.course = course;

      ({
        name: this.title,
        length: this.duration,
        description: this.description,
        date: this.date
      } = this.course);

      this.setForm();
    });
  }

  submitForm() {
    ({
      title: this.title,
      description: this.description,
      duration: this.duration,
      date: this.date
    } = this.courseForm.value);

    if (!this.course) {
      const newCourse = new Course(nanoid(), this.title, this.duration, this.description, moment(this.date, this.dateFormat).toDate());
      this.store.dispatch(new courseStore.AddCourse(newCourse));
    } else {
      this.course.name = this.title;
      this.course.length = this.duration;
      this.course.description = this.description;
      this.course.date = this.date;
      this.store.dispatch(new courseStore.EditCourse(this.course));
    }
  }

}
