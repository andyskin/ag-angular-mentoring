import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'course-date',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateInputComponent),
      multi: true
    }
  ]
})
export class DateInputComponent implements ControlValueAccessor {
  @Input() invalid: boolean;

  private onChange = (_: any) => {};
  private onTouch = () => {};
  private dateFormat = 'MM/DD/YYYY';
  public date: string;

  constructor() { }

  writeValue(value: any) {
    if (value) {
      this.date = moment(value).format(this.dateFormat);
    }
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouch = fn;
  }

  handleInput() {
    this.onChange(this.date);
    this.onTouch();
  }

}
