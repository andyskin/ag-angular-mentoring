import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'course-duration',
  templateUrl: './duration-input.component.html',
  styleUrls: ['./duration-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DurationInputComponent),
      multi: true
    }
  ]
})
export class DurationInputComponent implements ControlValueAccessor {
  @Input() invalid: boolean;

  private onChange = (_: any) => {};
  private onTouch = () => {};
  public duration: string;
  
  constructor() { }

  writeValue(value: any) {
    this.duration = value;
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouch = fn;
  }

  handleInput() {
    this.onChange(this.duration);
    this.onTouch();
  }

}
