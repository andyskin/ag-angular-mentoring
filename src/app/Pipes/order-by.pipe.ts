import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(array: any[], orderField: any): any {
    return array.sort((a: any, b: any) => {
      const aField = a[orderField],
        bField = b[orderField];

      if(typeof(aField.getDate) === 'function') {
        return this.compareDates(aField, bField);
      } 
      else {
        if (aField > bField) return -1;
        else if (aField < bField) return 1;
      }

      return 0;
    });
  }

  private compareDates(date1: Date, date2: Date) {
    const date1Ms = date1.getTime(),
      date2Ms = date2.getTime();

    if (date1Ms > date2Ms) return -1;
    else if (date1Ms < date2Ms) return 1;
    
    return 0;
  }

}
