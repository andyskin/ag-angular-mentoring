import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from '../types';

@Pipe({
  name: 'courseSearch'
})
export class CourseSearchPipe implements PipeTransform {

  transform(courses: ICourse[], courseName: string): ICourse[] {
    return courses.filter(
      course => course.name.toLowerCase().includes(courseName.toLocaleLowerCase())
    );
  }

}
