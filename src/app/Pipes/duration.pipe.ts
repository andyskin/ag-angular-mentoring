import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(duration: number): string {
    if (duration < 60) return `${duration}min`;

    const hours = Math.floor(duration / 60),
      minutes = duration - (hours * 60);

    return `${hours}h ${minutes}min`;
  }

}
