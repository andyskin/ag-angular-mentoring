import { Component, OnInit } from '@angular/core';
import { ApiService } from './Services/api/api.service';
import { Store } from '@ngrx/store';
import { CoursesState, LoadCourses } from './Store/courses';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AgAngularMentoring';
  status: boolean;

  constructor(
    private api: ApiService,
    private store: Store<CoursesState>
  ) {}

  ngOnInit() {
    this.api.status$.subscribe((status:boolean) => {
      setTimeout(() => {
        this.status = status
      });
    });

    this.store.dispatch(new LoadCourses({}));
  }
}
