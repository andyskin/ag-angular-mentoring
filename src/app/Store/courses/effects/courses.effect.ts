import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import * as courseActions from '../actions';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { CoursesService } from '../../../Services/courses/courses.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class CoursesEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private coursesService: CoursesService
    ) { }

    @Effect()
    loadCourses$ = this.actions$.ofType(courseActions.LoadActionList.LOAD_COURSES).pipe(
        switchMap((state: courseActions.LoadCourses) => {
            const { start = 0, count = 6 } = state.payload;

            return this.coursesService.getCourses(start, count).pipe(
                map(courses => new courseActions.LoadCoursesSuccess(courses)),
                catchError(error => of(new courseActions.LoadCoursesFail(error)))
            )
        })
    );

    @Effect()
    addCourse$ = this.actions$.ofType(courseActions.AddActionList.ADD_COURSE).pipe(
        switchMap((state: courseActions.AddCourse) => {
            const newCourse = state.payload;

            return this.coursesService.addCourse(newCourse).pipe(
                map(() => new courseActions.AddCourseSuccess(newCourse)),
                catchError(error => of(new courseActions.AddCourseFail(error))),
                tap(() => { this.router.navigate(['/', 'courses']) })
            )
        })
    );

    @Effect()
    editCourse$ = this.actions$.ofType(courseActions.EditActionList.EDIT_COURSE).pipe(
        switchMap((state: courseActions.EditCourse) => {
            const editedCourse = state.payload;

            return this.coursesService.updateCourse(editedCourse).pipe(
                map(() => new courseActions.EditCourseSuccess(editedCourse)),
                catchError(error => of(new courseActions.EditCourseFail(error))),
                tap(() => { this.router.navigate(['/', 'courses']) })
            )
        })
    );

    @Effect()
    deleteCourse$ = this.actions$.ofType(courseActions.DeleteActionList.DELETE_COURSE).pipe(
        switchMap((state: courseActions.DeleteCourse) => {
            const deletedCourseId = state.payload;

            return this.coursesService.deleteCourse(deletedCourseId).pipe(
                map(() => new courseActions.DeleteCourseSuccess(deletedCourseId)),
                catchError(error => of(new courseActions.DeleteCourseFail(error))),
                tap(() => { this.router.navigate(['/', 'courses']) })
            )
        })
    )
}