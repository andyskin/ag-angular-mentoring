import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CoursesState } from './courses.state';
import * as loadStatus from './reducers/courses.load.reducer';
import { ICourse } from '../../types';

export const getCoursesState = createFeatureSelector<CoursesState>('courses');

export const getAllCourses = createSelector(
    getCoursesState,
    loadStatus.getCourses
);

export const getCourse = (id: string) => createSelector(
    getAllCourses,
    (courses: ICourse[]) => courses.find(
        course => course.id == id
    )
);

export const getCoursesLoaded = createSelector(
    getCoursesState,
    loadStatus.getCoursesLoaded
);

export const getCoursesLoading = createSelector(
    getCoursesState,
    loadStatus.getCoursesLoading
);

export * from './reducers';
export * from './actions';
export * from './effects';
export * from './courses.state';