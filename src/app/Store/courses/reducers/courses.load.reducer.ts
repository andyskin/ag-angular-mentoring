import { CoursesState, initialCoursesState } from "../courses.state";
import { LoadActionList, LoadCoursesAction } from '../actions/courses.load.action';

export function loadCoursesReducer (
    state: CoursesState = initialCoursesState,
    action: LoadCoursesAction
): CoursesState {
    switch (action.type) {
        case LoadActionList.LOAD_COURSES: {
            return {
                ...state,
                loading: true
            }
        }
        case LoadActionList.LOAD_COURSES_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
        case LoadActionList.LOAD_COURSES_SUCCESS: {
            return {
                courses: [...state.courses, ...action.payload],
                loading: false,
                loaded: true
            }
        }
    }

    return state;
}

export const getCourses = (state: CoursesState) => state.courses;
export const getCoursesLoading = (state: CoursesState) => state.loading;
export const getCoursesLoaded = (state: CoursesState) => state.loaded;