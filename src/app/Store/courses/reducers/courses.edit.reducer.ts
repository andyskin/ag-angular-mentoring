import { CoursesState, initialCoursesState } from "../courses.state";
import { EditActionList, EditCourseAction } from '../actions/courses.edit.action';
import { ICourse } from "../../../types";

const updateCourses = (state: CoursesState, editedCourse: ICourse): ICourse[] => {
    const oldCourses = [...state.courses],
    oldCourseIndex = oldCourses.indexOf(state.courses.find(
        course => course.id === editedCourse.id
    ));

    oldCourses[oldCourseIndex] = editedCourse;
    return oldCourses;
};

export function editCourseReducer (
    state: CoursesState = initialCoursesState,
    action: EditCourseAction
): CoursesState {
    switch (action.type) {
        case EditActionList.EDIT_COURSE: {
            return {
                ...state,
                loading: true
            }
        }
        case EditActionList.EDIT_COURSE_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
        case EditActionList.EDIT_COURSE_SUCCESS: {
            return {
                courses: updateCourses(state, action.payload),
                loading: false,
                loaded: true
            }
        }
    }

    return state;
}