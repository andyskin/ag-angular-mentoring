import { addCourseReducer } from './courses.add.reducer';
import { editCourseReducer } from './courses.edit.reducer';
import { loadCoursesReducer } from './courses.load.reducer';
import { deleteCourseReducer } from './courses.delete.reducer';
import reduceReducers from 'reduce-reducers';

export const courseReducer = reduceReducers(
    loadCoursesReducer,
    addCourseReducer,
    editCourseReducer,
    deleteCourseReducer
);

export * from './courses.add.reducer';
export * from './courses.load.reducer';
export * from './courses.edit.reducer';
export * from './courses.delete.reducer';