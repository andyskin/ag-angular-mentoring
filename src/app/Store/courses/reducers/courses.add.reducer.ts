import { CoursesState, initialCoursesState } from "../courses.state";
import { AddActionList, AddCourseAction } from '../actions/courses.add.action';

export function addCourseReducer (
    state: CoursesState = initialCoursesState,
    action: AddCourseAction
): CoursesState {
    switch (action.type) {
        case AddActionList.ADD_COURSE: {
            return {
                ...state,
                loading: true
            }
        }
        case AddActionList.ADD_COURSE_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
        case AddActionList.ADD_COURSE_SUCCESS: {
            return {
                courses: [...state.courses, action.payload],
                loading: false,
                loaded: true
            }
        }
    }

    return state;
}