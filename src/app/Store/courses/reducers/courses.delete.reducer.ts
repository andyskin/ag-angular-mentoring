import { CoursesState, initialCoursesState } from "../courses.state";
import { DeleteActionList, DeleteCourseAction} from '../actions/courses.delete.action';

const updatedCourses = (state: CoursesState, deletedCourseId: string) => {
    return state.courses.filter(
        course => course.id !== deletedCourseId
    );
};

export function deleteCourseReducer (
    state: CoursesState = initialCoursesState,
    action: DeleteCourseAction
): CoursesState {
    switch (action.type) {
        case DeleteActionList.DELETE_COURSE: {
            return {
                ...state,
                loading: true
            }
        }
        case DeleteActionList.DELETE_COURSE_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
        case DeleteActionList.DELETE_COURSE_SUCCESS: {
            return {
                courses: updatedCourses(state, action.payload),
                loading: false,
                loaded: true
            }
        }
    }

    return state;
}