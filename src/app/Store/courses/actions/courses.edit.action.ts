import { Action } from "@ngrx/store";
import { ICourse } from "../../../types";

export enum EditActionList {
    EDIT_COURSE = '[Courses] Edit Course',
    EDIT_COURSE_SUCCESS = '[Courses] Edit Course Success',
    EDIT_COURSE_FAIL = '[Courses] Edit Course Fail'
};

export class EditCourse implements Action {
    readonly type = EditActionList.EDIT_COURSE;
    constructor(public payload: ICourse) { }
};

export class EditCourseSuccess implements Action {
    readonly type = EditActionList.EDIT_COURSE_SUCCESS;
    constructor(public payload: ICourse) { }
};

export class EditCourseFail implements Action {
    readonly type = EditActionList.EDIT_COURSE_FAIL;
    constructor(public payload: any) { }
};

export type EditCourseAction = EditCourse | EditCourseSuccess | EditCourseFail;