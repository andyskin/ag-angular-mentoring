import { LoadCoursesAction } from "./courses.load.action";
import { AddCourseAction } from "./courses.add.action";
import { EditCourseAction } from "./courses.edit.action";
import { DeleteCourseAction } from "./courses.delete.action";

export type CoursesAction = LoadCoursesAction | AddCourseAction | EditCourseAction | DeleteCourseAction;

export * from './courses.add.action';
export * from './courses.load.action';
export * from './courses.edit.action';
export * from './courses.delete.action';
