import { Action } from "@ngrx/store";
import { ICourse } from "../../../types";

export enum AddActionList {
    ADD_COURSE = '[Courses] Add Course',
    ADD_COURSE_SUCCESS = '[Courses] Add Course Success',
    ADD_COURSE_FAIL = '[Courses] Add Course Fail'
};

export class AddCourse implements Action {
    readonly type = AddActionList.ADD_COURSE;
    constructor(public payload: ICourse) { }
};

export class AddCourseSuccess implements Action {
    readonly type = AddActionList.ADD_COURSE_SUCCESS;
    constructor(public payload: ICourse) { }
};

export class AddCourseFail implements Action {
    readonly type = AddActionList.ADD_COURSE_FAIL;
    constructor(public payload: any) { }
};

export type AddCourseAction = AddCourse | AddCourseSuccess | AddCourseFail;