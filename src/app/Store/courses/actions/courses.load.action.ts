import { Action } from "@ngrx/store";
import { ICourse } from "../../../types";

export enum LoadActionList {
    LOAD_COURSES = '[Courses] Load Courses',
    LOAD_COURSES_SUCCESS = '[Courses] Load Courses Success',
    LOAD_COURSES_FAIL= '[Courses] Load Courses Fail'
};

export class LoadCourses implements Action {
    readonly type = LoadActionList.LOAD_COURSES;
    constructor(public payload: {start?: number, count?: number}) {}
};

export class LoadCoursesSuccess implements Action {
    readonly type = LoadActionList.LOAD_COURSES_SUCCESS;
    constructor(public payload: ICourse[]) {}
};

export class LoadCoursesFail implements Action {
    readonly type = LoadActionList.LOAD_COURSES_FAIL;
    constructor(public payload: any) {}
};

export type LoadCoursesAction = LoadCourses | LoadCoursesSuccess | LoadCoursesFail;