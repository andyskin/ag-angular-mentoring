import { Action } from "@ngrx/store";

export enum DeleteActionList {
    DELETE_COURSE = '[Courses] Delete Course',
    DELETE_COURSE_SUCCESS = '[Courses] Delete Course Success',
    DELETE_COURSE_FAIL = '[Courses] Delete Course Fail'
};

export class DeleteCourse implements Action {
    readonly type = DeleteActionList.DELETE_COURSE;
    constructor(public payload: string) { }
};

export class DeleteCourseSuccess implements Action {
    readonly type = DeleteActionList.DELETE_COURSE_SUCCESS;
    constructor(public payload: string) { }
};

export class DeleteCourseFail implements Action {
    readonly type = DeleteActionList.DELETE_COURSE_FAIL;
    constructor(public payload: any) { }
};

export type DeleteCourseAction = DeleteCourse | DeleteCourseSuccess | DeleteCourseFail;