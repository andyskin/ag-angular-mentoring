import { ICourse } from "../../types";

export interface CoursesState {
    courses: ICourse[];
    loaded: boolean;
    loading: boolean;
};

export const initialCoursesState: CoursesState = {
    courses: [],
    loaded: false,
    loading: false
};