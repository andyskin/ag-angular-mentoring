import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import * as authActions from '../actions';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { AuthService } from '../../../Services/auth/auth.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private authService: AuthService
    ) { }

    @Effect()
    login$ = this.actions$.ofType(authActions.AuthActionList.LOGIN).pipe(
        switchMap((state: authActions.Login) => {
            const token: string = state.payload;

            return this.authService.getUserInfo(token).pipe(
                map(user => new authActions.LoginSuccess(user)),
                catchError(error => of(new authActions.LoginFail(error))),
                tap(() => { this.router.navigate(['/']) })
            )
        })
    );
}