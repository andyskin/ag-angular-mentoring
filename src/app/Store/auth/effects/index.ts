import { AuthEffects } from "./auth.effect";

export const authEffects: any[] = [AuthEffects];

export * from './auth.effect';