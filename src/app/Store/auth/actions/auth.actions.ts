import { Action } from "@ngrx/store";
import { IUser } from "../../../types";

export enum AuthActionList {
    LOGIN = '[Auth] Login',
    LOGIN_SUCCESS = '[Auth] Login Success',
    LOGIN_FAIL = '[Auth] Login Fail',
    LOGOUT = '[Auth] Logout'
};

export class Login implements Action {
    readonly type = AuthActionList.LOGIN;
    constructor(public payload: string) {}
}

export class LoginSuccess implements Action {
    readonly type = AuthActionList.LOGIN_SUCCESS;
    constructor(public payload: IUser) {}
}

export class LoginFail implements Action {
    readonly type = AuthActionList.LOGIN_FAIL;
    constructor(public payload: any) {}
}

export class Logout implements Action {
    readonly type = AuthActionList.LOGOUT;
    constructor() {}
}

export type AuthAction = Login | LoginSuccess | LoginFail | Logout;