import { AuthAction, AuthActionList} from '../actions';
import { AuthState, initialAuthState } from '../auth.state';

export function authReducer (
    state: AuthState = initialAuthState,
    action: AuthAction
) {
    switch(action.type) {
        case AuthActionList.LOGIN: {
            return {
                ...state,
                loading: true,
                loaded: false
            }
        }
        case AuthActionList.LOGIN_SUCCESS: {
            return {
                user: action.payload,
                loading: false,
                loaded: true
            }
        }
        case AuthActionList.LOGIN_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
        case AuthActionList.LOGOUT: {
            return {
                user: null,
                loading: false,
                loaded: false
            }
        }
    }

    return state;
}

export const getUser = (state: AuthState) => state.user;
export const getUserLoading = (state: AuthState) => state.loading;
export const getUserLoaded = (state: AuthState) => state.loaded;