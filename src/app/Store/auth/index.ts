import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AuthState } from './auth.state';
import * as authStatus from './reducers/auth.reducer';

export const getUserState = createFeatureSelector('users');

export const getAuthState = createSelector(
    getUserState,
    (state: AuthState) => state.user
);

export const getUser = createSelector(
    getUserState,
    authStatus.getUser
);

export * from './reducers';
export * from './actions';
export * from './effects';
export * from './auth.state';