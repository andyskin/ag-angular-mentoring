import { IUser } from "../../types";

export interface AuthState {
    user: IUser;
    loaded: boolean;
    loading: boolean;
};

export const initialAuthState: AuthState = {
    user: null,
    loaded: false,
    loading: false
};