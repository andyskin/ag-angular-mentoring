import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';
import { BreadcrumbsComponent } from '../../Components/breadcrumbs/breadcrumbs.component';
import { CourseListComponent } from '../../Pages/course-list/course-list.component';
import { CoursesToolboxComponent } from '../../Components/courses-toolbox/courses-toolbox.component';
import { CourseComponent } from '../../Components/course/course.component';
import { FormsModule } from '@angular/forms';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        MainComponent,
        BreadcrumbsComponent,
        CourseListComponent,
        CoursesToolboxComponent,
        CourseComponent
      ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
