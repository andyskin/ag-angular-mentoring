import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../Services/auth/auth.service';
import { Store } from '@ngrx/store';

import * as authStore from '../../Store/auth';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {

  public user: string;

  constructor(
    private authService: AuthService,
    private store: Store<authStore.AuthState>
  ) {
    this.user = null;
  }

  ngOnInit() {
   this.authService.user$.subscribe(user => {
     setTimeout(() => {
      this.user = user
     });
   });
  }

  logout() {
    this.store.dispatch(new authStore.Logout());
    this.authService.logout();
  }

}
