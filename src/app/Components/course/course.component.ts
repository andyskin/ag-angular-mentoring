import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ICourse } from '../../types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  @Input() course: ICourse;
  @Output() deleteCourse = new EventEmitter();
  
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  editCourse(courseId: string) {
    this.router.navigateByUrl(`/courses/${courseId}`);
  }

  removeCourse(courseId: string) {
    this.deleteCourse.emit(courseId);
  }

}
