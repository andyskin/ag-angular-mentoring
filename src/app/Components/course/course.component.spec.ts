import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseComponent } from './course.component';
import { ICourse } from '../../types';
import { Component } from '@angular/core';

@Component({
  template: `
    <app-course 
      [course]="course" (deleteCourse)="deleteCourse($event)">
    </app-course>
  `
})
class TestHostComponent {
  course: ICourse = {
    "id": "1",
    "name": "Test course 1",
    "date": new Date(),
    "length": 60,
    "description": "Test course 1 description",
    "isTopRated": false,
    "authors": []
  };

  idToDelete: string;

  deleteCourse(courseId: string) {
    this.idToDelete = courseId;
  }
}

describe('CourseComponent', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let testHost: TestHostComponent;
  let courseEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseComponent, TestHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;
    courseEl = fixture.nativeElement.querySelector('.course');
    fixture.detectChanges();
  });

  it('should display course title', () => {
    const expectedTitle = testHost.course.name,
      realTitle = courseEl.querySelector('.course-title').textContent;

    expect(realTitle).toContain(expectedTitle);
  });

  it('should have an image inside', () => {
    expect(courseEl.querySelector('img.course-logo')).toBeTruthy();
  });

  it('should have Edit button inside', () => {
    expect(courseEl.querySelector('.edit-course')).toBeTruthy();
  });

  it('should have Delete button inside', () => {
    expect(courseEl.querySelector('.remove-course')).toBeTruthy();
  });

  it('\'s delete button should initiate course removal when clicked', () => {
    const deleteBtn: HTMLButtonElement = courseEl.querySelector('.remove-course');
    deleteBtn.click();
    expect(testHost.idToDelete).toContain(testHost.course.id);
  });
});
