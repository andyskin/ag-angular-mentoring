import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {

  public breadcrumbs: object[];
  private config: Map<any, any>;

  constructor(
    private router: Router
  ) {
    this.breadcrumbs = [];
    this.config = new Map();
  }

  ngOnInit() {
    this.config
    .set("courses", {
      label: 'Courses',
      url: '/courses'
    })
    .set("login", {
      label: 'Login',
      url: '/login'
    })
    .set('404', {
      label: 'Not found',
      url: '/404'
    })
    .set(null, {
      label: 'Video Course '
    });
  }

  ngDoCheck() {
    this.transformUrlToBreadcrumbs();
  }

  transformUrlToBreadcrumbs() {
    this.breadcrumbs = [];

    const currentPath = this.router.url.split("/");
    for (let pathSegment of currentPath) {
      if (!pathSegment) continue;

      const breadcrumb = this.config.get(pathSegment) || {};
      if (!breadcrumb.label) {
        Object.assign(breadcrumb, this.config.get(null));
        breadcrumb.label = breadcrumb.label + pathSegment;
      }

      this.breadcrumbs.push(breadcrumb);
    }
  }

}
