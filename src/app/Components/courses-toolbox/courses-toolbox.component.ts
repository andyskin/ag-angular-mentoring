import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-courses-toolbox',
  templateUrl: './courses-toolbox.component.html',
  styleUrls: ['./courses-toolbox.component.css']
})
export class CoursesToolboxComponent implements OnInit {
  @Output() findCourse = new EventEmitter();

  public searchForm: FormGroup;
  courseName$: Subject<string>;

  constructor() { 
    this.courseName$ = new Subject<string>();
    this.searchForm = new FormGroup({
      query: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  ngOnInit() {
    this.courseName$.pipe(
      debounceTime(500)
    ).subscribe(searchQuery => {
      this.findCourse.emit(searchQuery)
    });
  }

  changeQuery() {
    if (this.searchForm.valid) {
      this.courseName$.next(this.searchForm.value.query);
    } else {
      this.findCourse.emit();
    }
  }

}
