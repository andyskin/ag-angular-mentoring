import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { CoursesToolboxComponent } from './courses-toolbox.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('CoursesToolboxComponent', () => {
  let component: CoursesToolboxComponent;
  let fixture: ComponentFixture<CoursesToolboxComponent>;
  let spy: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesToolboxComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('search field should have the same value as #courseName', () => {
    const searchField = fixture.nativeElement.querySelector('.course-search'),
      searchValue = 'Aloha';

    searchField.value = searchValue;
    searchField.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(searchField.value).toBe(searchValue);
  });

  it('search button should execute #findCourse', () => {
    const searchBtn: HTMLButtonElement = fixture.nativeElement.querySelector('.searchBtn');

    spy = spyOn(component, 'findCourse');
    searchBtn.click();
    expect(component.findCourse).toHaveBeenCalled();
  });
});
