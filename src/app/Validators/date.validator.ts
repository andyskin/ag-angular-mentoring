import { ValidatorFn, AbstractControl } from "@angular/forms";
import * as moment from 'moment';

export function dateFormatValidator(format: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        return (!format.test(control.value))
            ? { 'incorrectFormat': {value: control.value} }
            : null;
    };
}

export function dateLengthValidator(length: number): ValidatorFn {
    return (control: AbstractControl): {[key: string] : any} | null => {
        return (!control.value || control.value.length !== length)
            ? { 'incorrectLength': {value: control.value} }
            : null;
    };
}

export function dateExistsValidator(format: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        return (!moment(control.value, format).isValid())
            ? { 'incorrectValue': {value: control.value} }
            : null;
    };
}