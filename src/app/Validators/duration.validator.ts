import { ValidatorFn, AbstractControl } from "@angular/forms";

export function durationTypeValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        return (isNaN(parseInt(control.value)))
            ? { 'incorrectType': {value: control.value} }
            : null;
    };
}