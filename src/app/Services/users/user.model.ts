import { IUser, IName } from "../../types";

export class User implements IUser {
    id: string;
    fakeToken: string;
    name: IName;
    login: string;
    password: string;
    token: string;

    constructor(id, name, login, password) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
    }
}
