import { ICourse, IAuthor } from "../../types";

export class Course implements ICourse {
    id: string;
    name: string;
    date: Date;
    length: number;
    description: string;
    isTopRated: boolean;
    authors: IAuthor[];

    constructor(id, name, length, description, date) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.length = length;
        this.description = description;
        this.isTopRated = false;
        this.authors = [];
    }
}
