import { Injectable } from '@angular/core';
import { ICourse } from '../../types';
import { ApiService } from '../api/api.service';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(
    private api: ApiService
  ) {}

  public getCourses = (start: number, count?: number) => this.api.getCourses(start, count);
  public getCourse = (id: string) => this.api.getCourse(id);
  public findCourse = (textFragment: string) => this.api.findCourse(textFragment);
  public addCourse = (course: ICourse) => this.api.addCourse(course);
  public deleteCourse = (id: string) => this.api.deleteCourse(id);
  public updateCourse = (course: ICourse) => this.api.updateCourse(course);

  public fixDates = (courses: ICourse[]) => {
    courses.forEach(course => {
      course.date = new Date(course.date);
    });
  }
}
