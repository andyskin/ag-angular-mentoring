import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';

import { environment } from '../../../environments/environment'
import { IUserResponse, IUser, ICourse } from '../../types';
import { map, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private authUrl: string;
  private coursesUrl: string;
  private token: string;
  private status: BehaviorSubject<boolean>;
  public status$: Observable<boolean>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.authUrl = `${environment.JSON_SERVER}/auth`;
    this.coursesUrl = `${environment.JSON_SERVER}/courses`;
    this.status = new BehaviorSubject<boolean>(true);
    this.status$ = this.status.asObservable();
  }

  private processResponse(response: any) {
    this.setStatus(true);
    return response;
  }

  public setStatus(status: boolean) {
    this.status.next(status);
  }

  public setToken = (token: string) => {
    this.token = token;
  }

  public getToken = () => this.token;

  public eraseToken = () => {
    this.token = null;
  }

  public login = (username: string, password: string): Observable<IUserResponse> => {
    return this.http.post<IUserResponse>(`${this.authUrl}/login`, {
      "login": username,
      "password": password
    }).pipe(
      delay(750),
      map(response => this.processResponse(response))
    )
  }

  public getUserInfo = (token: string): Observable<IUser> => {
    this.setToken(token);
    return this.http.post<IUser>(`${this.authUrl}/userinfo`, null)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public getCourses = (start: number = 0, count: number = 6): Observable<ICourse[]> => {
    const requestURL = `${this.coursesUrl}?start=${start}&count=${count}`;
    return this.http.get<ICourse[]>(requestURL)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public getCourse = (id: string): Observable<ICourse> => {
    return this.http.get<ICourse>(`${this.coursesUrl}/${id}`)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public findCourse = (textFragment: string): Observable<ICourse[]> => {
    return this.http.get<ICourse[]>(`${this.coursesUrl}?textFragment=${textFragment}`)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public addCourse = (course: ICourse): Observable<Object> => {
    return this.http.post(this.coursesUrl, course)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public deleteCourse = (id: string): Observable<Object> => {
    return this.http.delete(`${this.coursesUrl}/${id}`)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };

  public updateCourse = (course: ICourse): Observable<Object> => {
    return this.http.put(`${this.coursesUrl}/${course.id}`, course)
      .pipe(
        delay(750),
        map(response => this.processResponse(response))
      )
  };
}
