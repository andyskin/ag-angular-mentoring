import { Injectable } from '@angular/core';
import { IUser, IUserResponse } from '../../types';
import { Observable, ReplaySubject, BehaviorSubject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: BehaviorSubject<string>;
  private token: string;
  public user$: Observable<string>;

  constructor(
    private api: ApiService,
    private router: Router
  ) {
    const currentUser = this.getCurrentUserFromStorage();
    this.user = new BehaviorSubject<string>(
       currentUser ? currentUser.login : null
    );
    this.user$ = this.user.asObservable();
  }

  public login = (username: string, password: string) => this.api.login(username, password);
  public getUserInfo = (token: string) => this.api.getUserInfo(token);

  public logout = (): void => {
    this.user.next(null);
    localStorage.removeItem('currentUser');
    this.api.eraseToken();
    this.router.navigate(["/login"]);
  }

  public setCurrentUser(user: IUser) {
    if (user) {
      this.user.next(user.login);
      this.token = user.fakeToken;
      localStorage.setItem('currentUser', JSON.stringify(user));
    }
  }

  public getCurrentUserFromStorage() {
    return JSON.parse(localStorage.getItem('currentUser')) || null;
  }
}
